package com.hackus.learn.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZuulServiceApp {

    public static void main(String[] args) {
        SpringApplication.run(ZuulServiceApp.class, args);
    }
}
